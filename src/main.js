console.log("Hello world!");

/*
    window.openMobile ?!
    In a module context, variables don't automatically get declared globally. 
    You'll have to attach them to window yourself. 
    This is to prevent the scope ambiguity issues that you'd run into when using normal script tags. 
*/
window.openMobile = function openMobile() {
  document.querySelector(".menu").classList.toggle("open");
};
