# Workshop: Create responsive navigation with Flexbox

MS Teams Recording - [LINK](https://blubitoag-my.sharepoint.com/personal/p_ivanova_blubito_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fp%5Fivanova%5Fblubito%5Fcom%2FDocuments%2FAufnahmen%2FFront%2Dend%20guild%20%2D%20Regular%20Meeting%2D20211019%5F110239%2DMeeting%20Recording%2Emp4&parent=%2Fpersonal%2Fp%5Fivanova%5Fblubito%5Fcom%2FDocuments%2FAufnahmen&originalPath=aHR0cHM6Ly9ibHViaXRvYWctbXkuc2hhcmVwb2ludC5jb20vOnY6L2cvcGVyc29uYWwvcF9pdmFub3ZhX2JsdWJpdG9fY29tL0VSTDVjWk50Y1VCSXVDX1VOTndYR0hVQjF1UGxiNXZhRmQ2UGlESXlESVVzUEE%5FcnRpbWU9S0pjUmktS1MyVWc) . Or you can find it in our MS Teams Group -> Frontend Guild -> Meeting Notes and Recordings (in the tabs above)


## How to use this project

### Install dependencdies

```
yarn instell
OR
npm install
```

### Start the project

```
yarn dev
OR
npm run dev
```

## ParcelJS

This project uses [ParcelJS](https://parceljs.org/getting-started/webapp/) as a build tool. Parcel uses by default the new `script type="module"` extention, which introduces a small change in our JS files - when we have modules we need to specifically attach our functions to the `window` element. A small comment about this is inside `main.js`
